package io.techniktom.currencycalculator.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;
import java.net.URL;

import io.techniktom.currencycalculator.api.models.Currencies;

public class ApiClient {
    private static final String CURRENCIES_URL = "https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.xml";
    private static ApiClient INSTANCE;
    private static final ObjectMapper xmlMapper = new XmlMapper();

    public static ApiClient getInstance() {
        return (INSTANCE == null) ? (INSTANCE = new ApiClient()) : INSTANCE;
    }

    private ApiClient() {}

    public Currencies getCurrencies() throws IOException {
        return xmlMapper.readValue(new URL(CURRENCIES_URL).openStream(), Currencies.class);
    }
}
