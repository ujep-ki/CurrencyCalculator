package io.techniktom.currencycalculator.tools;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.Nullable;

public class CurrencyProvider extends ContentProvider {
    private final static String AUTHORITY = "io.techniktom.currency-provider";
    private final static int DB_VERSION = 1;
    private final static String TABLE_NAME = "currencies";
    private final static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    private final OpenHelper helper;

    private final static int CURRENCY_MATCH = 1;

    public CurrencyProvider() {
        helper = new OpenHelper(getContext());

        matcher.addURI(AUTHORITY, TABLE_NAME, CURRENCY_MATCH);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
//        switch (matcher.match(uri)) {
//
//        }

        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if(matcher.match(uri) == CURRENCY_MATCH) {
            long  id = helper.makeInsert(values);

            if(id != -1){
                Uri insertedUri = ContentUris.withAppendedId(uri, id);
                getContext().getContentResolver().notifyChange(uri, null);
                return insertedUri;
            }
        }

        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    @Override
    public boolean onCreate() {



        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if(matcher.match(uri) == CURRENCY_MATCH) {
            Cursor c = helper.makeQuery(projection, selection, selectionArgs, sortOrder);
            c.setNotificationUri(getContext().getContentResolver(), uri);

            return c;
        }

        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if(matcher.match(uri) == CURRENCY_MATCH)
            return helper
                    .getWritableDatabase()
                    .update(TABLE_NAME, values, selection, selectionArgs);


        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    private static class OpenHelper extends SQLiteOpenHelper {

        private static final String ID = "_id";
        private static final String CODE = "code";
        private static final String NAME = "name";
        private static final String AMOUNT = "amount";
        private static final String RATE = "rate";
        private static final String COUNTRY = "country";

        public OpenHelper(@Nullable Context context) {
            super(context, TABLE_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String query =
                    "create table %s(" +
                        "%s INTEGER AUTO INCREMENT UNIQUE, " +
                        "%s CHAR(3) not null, " +
                        "%s CLOB not null, " +
                        "%s INTEGER not null, " +
                        "%s real not null, " +
                        "%s CLOB NOT NULL" +
                    ");";

            db.execSQL(String.format(query, TABLE_NAME, ID, CODE, NAME, AMOUNT, RATE, COUNTRY));
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

        public Cursor makeQuery(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
            SQLiteQueryBuilder quarryBuilder = new SQLiteQueryBuilder();
            SQLiteDatabase db = getReadableDatabase();

            quarryBuilder.setTables(TABLE_NAME);

            return quarryBuilder
                    .query(
                            db,
                            projection,
                            selection,
                            selectionArgs,
                            null,
                            null,
                            sortOrder
                    );
        }

        public long makeInsert(ContentValues values) {
            return getWritableDatabase().insert(TABLE_NAME, CODE, values);
        }
    }
}