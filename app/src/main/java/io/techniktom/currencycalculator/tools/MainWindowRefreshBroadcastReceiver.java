package io.techniktom.currencycalculator.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TableLayout;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import io.techniktom.currencycalculator.activities.CurrencyConverterActivity;
import io.techniktom.currencycalculator.api.models.Currency;
import io.techniktom.currencycalculator.db.repository.Repositories;

public class MainWindowRefreshBroadcastReceiver extends BroadcastReceiver {
    private final TableLayout tableLayout;
    private final SwipeRefreshLayout swipeRefreshLayout;

    public MainWindowRefreshBroadcastReceiver(TableLayout tableLayout, SwipeRefreshLayout swipeRefreshLayout) {
        this.tableLayout = tableLayout;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        new Thread(this::executeUpdate).start();
    }

    private void executeUpdate() {
        final BiConsumer<Currency, View> onClick = (c, v) -> {
            Context context = tableLayout.getContext();
            Intent intent = new Intent(context, CurrencyConverterActivity.class);
            intent.putExtra("code", c.getCode());
            intent.putExtra("rate", c.getRate()/c.getAmount());
            context.startActivity(intent);
        };

        final List<View> currencies =  Repositories
                .getCurrencyRepository()
                .getAll()
                .stream()
                .map(c -> Tools.createTableRow(tableLayout.getContext(), c, onClick))
                .collect(Collectors.toList());

        tableLayout.post(() -> {
            tableLayout.removeAllViews();
            currencies.forEach(tableLayout::addView);

            if(swipeRefreshLayout.isRefreshing())
                Toast.makeText(tableLayout.getContext(), "Refreshed", Toast.LENGTH_SHORT).show();

            swipeRefreshLayout.setRefreshing(false);
        });
    }
}
