package io.techniktom.currencycalculator.tools;

public class Intents {
    public static final String LOAD_CURRENCIES = "io.techniktom.currency.LOAD_CURRENCIES";
    public static final String RELOAD_CURRENCIES = "io.techniktom.currency.RELOAD_CURRENCIES";
}
