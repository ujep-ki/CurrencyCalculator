package io.techniktom.currencycalculator.services;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import io.techniktom.currencycalculator.api.ApiClient;
import io.techniktom.currencycalculator.api.models.Currency;
import io.techniktom.currencycalculator.db.repository.CurrencyRepository;
import io.techniktom.currencycalculator.db.repository.Repositories;
import io.techniktom.currencycalculator.tools.Intents;

public class DataRefreshService extends JobIntentService {
    private static final int JOB_ID = 1000;
    private static final CurrencyRepository currencyRepository = Repositories.getCurrencyRepository();

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, DataRefreshService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        sendBroadcast(new Intent(Intents.RELOAD_CURRENCIES));

        if(!intent.getAction().equals(Intents.LOAD_CURRENCIES)) {
            try {
                List<Currency> currencies = ApiClient.getInstance().getCurrencies().getTable().getCurrencies();

                for (Currency currency : currencies) {
                    Optional<Currency> optionalCurrency = currencyRepository.getByCode(currency.getCode());

                    if (optionalCurrency.isPresent()) {
                        Currency localCurrency = optionalCurrency.get();

                        localCurrency.setAmount(currency.getAmount());
                        localCurrency.setCountry(currency.getCountry());
                        localCurrency.setCurrency(currency.getCurrency());
                        localCurrency.setRate(currency.getRate());

                        currencyRepository.update(localCurrency);
                    } else {
                        currencyRepository.insert(currency);
                    }
                }

                sendBroadcast(new Intent(Intents.RELOAD_CURRENCIES));
            } catch (IOException e) {
                new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(this, "Something happened with internet.\\n                  Try again later.", Toast.LENGTH_LONG).show());
            }
        }
    }
}
